'use strict';
(function() {
  /**
     * @ngdoc component
     * @name app.component:abNewAbout
     *
     * @description
     * Feature new about. Load all necesary data for his childs components
     */
  angular
    .module('app')
    .component('abNewAbout', {
      templateUrl: 'app/features/ab-new-about/ab-new-about.html',
      controller: NewAboutController,
      controllerAs: 'vm'
    });

  /** @ngInject */
  function NewAboutController(navbarPaths, guideItems, creation) {
    var vm = this;
    vm.pageData = {
      section: 'panel',
      sectionH1: 'Title H1',
      sectionText: 'New About Section',
      creationDate: creation.getDate(),
      contentType: 'guide-item',
      contentData: guideItems.getItems(),
      logoURL: 'assets/images/serenity/serenity.png',
      brand: 'Angular Basic',
      paths: navbarPaths.getPaths()
    };
  }

})();