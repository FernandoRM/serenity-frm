'use strict';
(function() {

  angular
    .module('app')
    .config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
      .state('new-about', {
        url: '/new-about',
        template: '<ab-new-about></ab-new-about>'
      });
  }

})();
