'use strict';
(function() {

/**
 * @ngdoc component
 * @name app.component:abCoverImage
 *
 * @description
 * A description of the component
 *
 * @param {object}  section       A field object
 * @param {string}  sectionText   A field object
 * @param {string}  sectionH1     A field object
 * @param {string}  logoUrl       A image url
 *
 */
  angular
    .module('app')
    .component('abCoverImage', {
      templateUrl: 'app/components/ab-cover-image/ab-cover-image.html',
      controller: CoverImageController,
      controllerAs: 'vm',
      bindings:{
        section: '<',
        sectionH1: '<',
        sectionText: '<',
        logoUrl: '<'
      }
    });

  /** @ngInject */
  function CoverImageController() {
    var vm = this;
    vm.section = vm.section;
  }

})();