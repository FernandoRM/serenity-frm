'use strict';
(function() {

/**
 * @ngdoc component
 * @name app.component:abPanel
 *
 * @description
 * A description of the component
 *
 * @param {object}  section       A field object
 * @param {string}  sectionText   A field object
 * @param {string}  sectionH1     A field object
 * @param {string}  logoUrl       A image url
 *
 */
  angular
    .module('app')
    .component('abPanel', {
      templateUrl: 'app/components/ab-panel/ab-panel.html',
      controller: PanelController,
      controllerAs: 'vm',
      bindings:{
        section: '<',
        sectionH1: '<',
        sectionText: '<',
        logoUrl: '<'
      }
    });

  /** @ngInject */
  function PanelController() {
    var vm = this;
    vm.section = vm.section;
    vm.cb1 = true;
    vm.cb2 = false;
  }

})();