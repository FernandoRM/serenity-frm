'use strict';
(function() {

  describe('Panel component', function() {

    var $componentController;

    beforeEach(module('app'));
    beforeEach(inject(function(_$componentController_) {
      $componentController = _$componentController_;
    }));

    it('should have a home section defined', function() {
      // Here we can passing actual bindings
      var vm = $componentController('Panel', null);
      expect(vm.section).toEqual(jasmine.any(String));
    });

    it('should have a about section defined', function() {
      // Here we can passing actual bindings to the component
      var vm = $componentController('Panel', null);
      expect(vm.section).toEqual(jasmine.any(String));
      expect(vm.section).toEqual('new-about');
    });

  });
})();